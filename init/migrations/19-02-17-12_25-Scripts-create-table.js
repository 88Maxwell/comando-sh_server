module.exports = {
    up : (queryInterface, Sequelize) => {
        return queryInterface.createTable("Scripts", {
            id           : { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
            collectionId : { type: Sequelize.UUID, allowNull: false, references: { model: "Collections", key: "id" } },
            executorId   : { type: Sequelize.UUID, references: { model: "Executors", key: "id" } },
            data         : { type: Sequelize.STRING, allowNull: false },
            status       : { type: Sequelize.ENUM, values: [ "SUCCESSFUL", "FAILED", "PENDING" ], defaultValue: "PENDING" },
            number       : { type: Sequelize.INTEGER, allowNull: true },
            createdAt    : { type: Sequelize.DATE, allowNull: false },
            updatedAt    : { type: Sequelize.DATE, allowNull: false }
        });
    },

    down : queryInterface => {
        return queryInterface.dropTable("Scripts");
    }
};
