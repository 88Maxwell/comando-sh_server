module.exports = {
    up : (queryInterface, Sequelize) => {
        return queryInterface.createTable("Collections", {
            id        : { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
            roomId    : { type: Sequelize.UUID, allowNull: false, references: { model: "Rooms", key: "id" } },
            status    : { type: Sequelize.ENUM, values: [ "SUCCESSFUL", "FAILED", "PENDING" ], defaultValue: "PENDING" },
            createdAt : { type: Sequelize.DATE, allowNull: false },
            updatedAt : { type: Sequelize.DATE, allowNull: false }
        });
    },

    down : queryInterface => {
        return queryInterface.dropTable("Collections");
    }
};
