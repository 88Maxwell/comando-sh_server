module.exports = {
    up : (queryInterface, Sequelize) => {
        return queryInterface.createTable("Executors", {
            id          : { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
            roomId      : { type: Sequelize.UUID, allowNull: false, references: { model: "Rooms", key: "id" } },
            name        : { type: Sequelize.STRING },
            email       : { type: Sequelize.STRING, allowNull: false },
            ip          : { type: Sequelize.STRING, allowNull: false },
            isConnected : { type: Sequelize.BOOLEAN, defaultValue: false },
            createdAt   : { type: Sequelize.DATE, allowNull: false },
            updatedAt   : { type: Sequelize.DATE, allowNull: false }
        });
    },

    down : queryInterface => {
        return queryInterface.dropTable("Executors");
    }
};
