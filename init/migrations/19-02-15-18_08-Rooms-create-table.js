module.exports = {
    up : (queryInterface, Sequelize) => {
        return queryInterface.createTable('Rooms', {
            id           : { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
            userId       : { type: Sequelize.UUID, references: { model: "Users", key: "id" } },
            name         : { type: Sequelize.STRING, allowNull: false},
            passwordHash : { type: Sequelize.STRING, allowNull: false },
            status       : { type: Sequelize.ENUM, values: [ "STOPED", "ACTIVE" ], defaultValue: "STOPED" },
            createdAt    : { type: Sequelize.DATE, allowNull: false },
            updatedAt    : { type: Sequelize.DATE, allowNull: false }
        });
    },

    down : queryInterface => {
        return queryInterface.dropTable('Rooms');
    }   
};
