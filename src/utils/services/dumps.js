export const user = obj => ({
    id    : obj.id,
    name  : obj.name,
    email : obj.email
});

export const room = obj => ({
    id        : obj.id,
    userId    : obj.userId,
    name      : obj.name,
    status    : obj.status,
    Executors : obj.Executors ? obj.Executors.map(executor) : []
});

export const collection = obj => ({
    id      : obj.id,
    roomId  : obj.roomId,
    status  : obj.status,
    Scripts : obj.Scripts ? obj.Scripts.map(script) : []
});

export const script = obj => ({
    id           : obj.id,
    collectionId : obj.collectionId,
    executorId   : obj.executorId,
    data         : obj.data,
    number       : obj.number,
    status       : obj.status
});

export const executor = obj => ({
    id          : obj.id,
    roomId      : obj.roomId,
    name        : obj.name,
    ip          : obj.ip,
    email       : obj.email,
    isConnected : obj.isConnected
});

