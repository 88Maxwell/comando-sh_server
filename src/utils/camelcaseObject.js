import camelcase from "camelcase";

export default function camelcaseObject(obj) {
    const resultObject = {};
    const keys = Object.keys(obj);

    keys.forEach(key => {
        const value = typeof obj[key] === "object" && !Array.isArray(obj[key])
            ? camelcaseObject(obj[key])
            : obj[key];

        resultObject[camelcase(key)] = value;
    });

    return resultObject;
}
