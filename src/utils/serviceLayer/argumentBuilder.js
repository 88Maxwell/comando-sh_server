import camelcaseObject from "../camelcaseObject";

export default {
    name    : "argumentBuilder",
    type    : "hidden",
    execute : ctx => {
        const args = camelcaseObject({
            ...ctx.request.body,
            ...ctx.params,
            ...ctx.query
        });

        return args;
    }
};

