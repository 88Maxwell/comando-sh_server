import validate from "./validate";
import argumentBuilder from "./argumentBuilder";
import permissionCheck from "./permissionCheck";

export function resolver(result) {
    this.body = result;
}

export const argumentsBuilder = args => args[0];

export const rules = {
    before : [ permissionCheck, argumentBuilder, validate ],
    after  : []
};
