import jwt           from "jsonwebtoken";
import { promisify } from "bluebird";
import { Exception } from "service-layer";
import sequelize from "../../sequelize";
import config from "../../../configs/config.json";

const jwtVerify = promisify(jwt.verify);
const User = sequelize.model("User");

export default {
    name    : "permissionCheck",
    type    : "custom",
    execute : async (ctx, args) => {
        if (args) {
            console.log("ctx.request.get: ", ctx.request.get);
            const token = ctx.request.get("X-AuthToken");
            const { id } = await jwtVerify(token, config.SECRET);
            const user = await User.findByPk(id);

            if (user) {
                /* eslint no-param-reassign: 0 */
                ctx.user = user;

                return ctx;
            }

            throw new Exception({
                code   : "PERMISSION_DENIED",
                fields : {
                    token : "WRONG_TOKEN"
                }
            });
        }
    }
};

