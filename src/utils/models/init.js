import models from "../../models";
import methods from "./methods";


export default (sequelize, Sequelize) => {
    const initList = [];

    // eslint-disable-next-line no-unused-vars
    for (const modelKey in models) {
        if (models.hasOwnProperty(modelKey)) {
            const model = models[modelKey](sequelize, Sequelize);

            if (model.hasOwnProperty("initRelation")) {
                initList.push(model);
            }
        }
    }

    initList.forEach(model =>  {
        model.initRelation();
        const modelName = model.options.name.singular;

        if (modelName === "User" || modelName === "Room") {
            methods.inject(model, methods.password);
        }
    });
};
