import http from "http";
import Koa from "koa";
// import socketIO from "socket.io";

import middlewares from "./middlewares";

const koaServer = new Koa();

koaServer.use(middlewares.helmet());
koaServer.use(middlewares.cors());
koaServer.use(middlewares.koaBody);
koaServer.use(middlewares.router.routes());

const server = http.createServer(koaServer.callback());

// export const io = socketIO(server);

// io.on("connection", socket => {
//     socket.on("example", msg => {
//         const rnd = Math.random() * msg;

//         console.log(rnd);
//         io.emit("example", rnd);
//     });
// });

export default server;
// export default koaServer;

