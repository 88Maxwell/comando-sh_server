import bluebird from "bluebird";
import config from "../configs/config.json";
import koaServer from "./server";

global.Promise = bluebird;

console.log(`APP STARTING AT PORT ${config.PORT}`);
export default koaServer.listen({ port: config.PORT });

