import Sequelize from "sequelize";
import dbConfig from "../configs/db.json";

import utils from "./utils/models";

const { POSTGRES_DB, POSTGRES_USER, POSTGRES_PASSWORD } = process.env;
const { database, username, password, dialect, host, port } = dbConfig[process.env.MODE];
const sequelize = new Sequelize(POSTGRES_DB || database, POSTGRES_USER || username, POSTGRES_PASSWORD || password, {
    insecureAuth : true,
    host,
    port,
    dialect
});

utils.init(sequelize, Sequelize);

export default sequelize;
