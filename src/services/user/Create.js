import { Service, Exception } from "service-layer";
import jwt from "jsonwebtoken";
import config from "../../../configs/config.json";
import sequelize from "../../sequelize";
import { user as dumpUser } from "../../utils/services/dumps";

const User = sequelize.model("User");

export default class Create extends Service {
    static validate = {
        data : [ "required", { "nested_object" : {
            "name"     : [ "string" ],
            "email"    : [ "required", "string", "email", "to_lc" ],
            "password" : [ "required", "string" ]
        } } ]
    };

    async execute({ data }) {
        const sameEmailUser = await User.findOne({
            where : { email: data.email }
        });

        if (sameEmailUser) {
            throw new Exception({
                code   : "INVALID_EMAIL",
                fields : {
                    email : "IS_EXIST"
                }
            });
        }

        const user = await User.create(data);

        return { token: jwt.sign(dumpUser(dumpUser(user)), config.SECRET) };
    }
}
