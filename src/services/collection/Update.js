import { Service } from "service-layer";
import sequelize from "../../sequelize";
import { collection as dumpCollection } from "../../utils/services/dumps";

const Collection = sequelize.model("Collection");

export default class Create extends Service {
    static validate = {
        "id"     : [ "required", "string" ],
        "roomId" : [ "required", "string" ],
        data     : [ "required", { "nested_object" : {
            "status" : [ "required", "string" ]
        } } ]

    };

    async execute({ id, data }) {
        const collection = await Collection.findByPk(id);
        const updatedCollection = await collection.update(data);

        return dumpCollection(updatedCollection);
    }
}
