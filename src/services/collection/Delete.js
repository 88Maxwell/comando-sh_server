import { Service } from "service-layer";
import sequelize from "../../sequelize";
import { collection as dumpCollection } from "../../utils/services/dumps";

const Collection = sequelize.model("Collection");

export default class Create extends Service {
    static validate = {
        id : [ "required", "string" ]
    };

    async execute({ id }) {
        const collection = await Collection.findByPk(id);

        await collection.destroy();

        return dumpCollection(collection);
    }
}
