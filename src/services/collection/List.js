import { Service } from "service-layer";
import sequelize from "../../sequelize";
import {
    collection as dumpCollection
    // script as dumpScript
} from "../../utils/services/dumps";

const Collection = sequelize.model("Collection");
const Script = sequelize.model("Script");

export default class List extends Service {
    static validate = {
        roomId : [ "required", "string" ]
    };

    async execute({ roomId }) {
        const collections = await Collection.findAll({
            where   : { roomId },
            include : Script
        });

        return collections.map(dumpCollection);
    }
}
