import { Service } from "service-layer";
import sequelize from "../../sequelize";
import { collection as dumpCollection } from "../../utils/services/dumps";

const Collection = sequelize.model("Collection");

export default class Create extends Service {
    static validate = {
        "roomId" : [ "required", "string" ]
    };

    async execute({ roomId }) {
        const collection = await Collection.create({ roomId });

        return dumpCollection(collection);
    }
}
