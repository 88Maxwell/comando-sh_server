import { Service } from "service-layer";
import sequelize from "../../sequelize";
import { room as dumpRoom } from "../../utils/services/dumps";

const Room = sequelize.model("Room");
const Collection = sequelize.model("Collection");
const Executor = sequelize.model("Executor");

export default class Create extends Service {
    static validate = {
        "id"     : [ "required", "string" ],
        "userId" : [ "required", "string" ]
    };

    async execute({ id, userId }) {
        const room = await Room.findOne({ where: { id, userId } });

        await Collection.destroy({ where: { roomId: room.id } });
        await Executor.destroy({ where: { roomId: room.id } });
        await room.destroy();


        return dumpRoom(room);
    }
}
