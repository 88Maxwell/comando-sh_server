import { Service } from "service-layer";
import sequelize from "../../sequelize";
import { room as dumpRoom } from "../../utils/services/dumps";

const Room = sequelize.model("Room");
const Executor = sequelize.model("Executor");

export default class Create extends Service {
    static validate = {
        "id"     : [ "required", "string" ],
        "userId" : [ "required", "string" ]
    };

    async execute({ id, userId }) {
        const room = await Room.findOne({
            where   : { id, userId },
            include : Executor
        });

        return dumpRoom(room);
    }
}
