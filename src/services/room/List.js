import { Service } from "service-layer";
import sequelize from "../../sequelize";
import { room as dumpRoom } from "../../utils/services/dumps";

const Room = sequelize.model("Room");
const Executor = sequelize.model("Executor");

export default class Create extends Service {
    static validate = {
        "userId" : [ "required", "string" ]
    };

    async execute({ userId }) {
        const rooms = await Room.findAll({
            include : Executor,
            where   : { userId }
        });

        return rooms.map(dumpRoom);
    }
}
