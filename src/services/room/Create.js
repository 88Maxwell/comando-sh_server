import { Service } from "service-layer";
import sequelize from "../../sequelize";
import { room as dumpRoom } from "../../utils/services/dumps";

const Room = sequelize.model("Room");

export default class Create extends Service {
    static validate = {
        "userId" : [ "required", "string" ],
        data     : [ "required", { "nested_object" : {
            "name"     : [ "required", "string" ],
            "password" : [ "required", "string" ],
            "status"   : [ "string" ]
        } } ]
    };

    async execute({ data, userId }) {
        const room = await Room.create({ ...data, userId });

        return dumpRoom(room);
    }
}
