import { Service, Exception } from "service-layer";
import sequelize from "../../sequelize";
import { room as dumpRoom } from "../../utils/services/dumps";

const Room = sequelize.model("Room");

export default class Login extends Service {
    static validate = {
        data : [ "required", { "nested_object" : {
            "name"     : [ "required", "string" ],
            "password" : [ "required", "string" ]
        } } ]
    };

    async execute({ data }) {
        const { name, password } = data;
        const room = await Room.findOne({ where: { name } });

        if (!(room && room.passwordHash && (await room.checkPassword(password)))) {
            throw new Exception({
                code   : "AUTHENTICATION_FAILED",
                fields : {
                    name     : "INVALID",
                    password : "INVALID"
                }
            });
        }

        return dumpRoom(room);
    }
}
