import { Service } from "service-layer";
import sequelize from "../../sequelize";
import { room as dumpRoom } from "../../utils/services/dumps";

const Room = sequelize.model("Room");

export default class Create extends Service {
    static validate = {
        "userId" : [ "required", "string" ],
        "roomId" : [ "required", "string" ],
        data     : [ "required", { "nested_object" : {
            "name"     : [ "string" ],
            "password" : [ "string" ],
            "status"   : [ "string" ]
        } } ]
    };

    async execute({ roomId, data }) {
        const room = await Room.findByPk(roomId);
        const updatedRoom = await room.update(data);

        return dumpRoom(updatedRoom);
    }
}
