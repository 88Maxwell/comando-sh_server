import { Service } from "service-layer";
import sequelize from "../../sequelize";
import { executor as dumpExecutor } from "../../utils/services/dumps";

const Executor = sequelize.model("Executor");

export default class Create extends Service {
    static validate = {
        "id"     : [ "required", "string" ],
        "roomId" : [ "required", "string" ],
        data     : [ "required", { "nested_object" : {
            "isConnected" : [ "required" ]
        } } ]
    };

    async execute({ data, id }) {
        const executor = await Executor.findByPk(id);
        const updatedExecutor = await executor.update(data);

        return dumpExecutor(updatedExecutor);
    }
}
