import { Service } from "service-layer";
import sequelize from "../../sequelize";
import { executor as dumpExecutor } from "../../utils/services/dumps";

const Executor = sequelize.model("Executor");

export default class Create extends Service {
    static validate = {
        "roomId" : [ "required", "string" ],
        data     : [ "required", { "nested_object" : {
            "name"  : [ "required", "string" ],
            "email" : [ "required", "string" ],
            "ip"    : [ "required", "string" ]
        } } ]
    };

    async execute({ data, roomId }) {
        const executor = await Executor.create({ ...data, roomId });

        return dumpExecutor(executor);
    }
}
