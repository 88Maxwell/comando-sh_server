import { Service } from "service-layer";
import sequelize from "../../sequelize";
import { script as dumpScript } from "../../utils/services/dumps";

const Script = sequelize.model("Script");

export default class Create extends Service {
    static validate = {
        "executorId"   : [ "required", "string" ],
        "collectionId" : [ "required", "string" ],
        data           : [ "required", { "nested_object" : {
            "data"   : [ "required", "string" ],
            "number" : [ "required", "integer" ]
        } } ]
    };

    async execute({ data, executorId, collectionId }) {
        const script = await Script.create({ ...data, collectionId, executorId });

        return dumpScript(script);
    }
}
