import { Service } from "service-layer";
import sequelize from "../../sequelize";
import { script as dumpScript } from "../../utils/services/dumps";

const Script = sequelize.model("Script");

export default class Show extends Service {
    static validate = {
        "executorId" : [ "required", "string" ]
    };

    async execute({ executorId }) {
        const scripts = await Script.findAll({
            where : { executorId, status: "PENDING" }
        });

        if (!!scripts && !scripts.length) return false;
        scripts.sort((a, b) => a.number - b.number);

        return dumpScript(scripts[0]);
    }
}
