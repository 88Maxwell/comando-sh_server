import { Service } from "service-layer";
import sequelize from "../../sequelize";
import { script as dumpScript } from "../../utils/services/dumps";

const Script = sequelize.model("Script");

export default class Create extends Service {
    static validate = {
        "scriptId" : [ "required", "string" ],
        data       : [ "required", { "nested_object" : {
            "status" : [ "required", "string" ]
        } } ]
    };

    async execute({ scriptId, data }) {
        const script = await Script.findByPk(scriptId);
        const updatedScript = await script.update(data);

        return dumpScript(updatedScript);
    }
}
