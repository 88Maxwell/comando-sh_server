import cors from "@koa/cors";
import helmet from "koa-helmet";
import koaBody from "./koaBody";
import router from "./router";

export default {
    koaBody,
    cors,
    helmet,
    router
};
