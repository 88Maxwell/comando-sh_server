import Router from "koa-router";
import ServiceLayer  from "../serviceLayer";

import UserCreate from "../services/user/Create";

import SessionCreate from "../services/session/Create";

import RoomCreate from "../services/room/Create";
import RoomUpdate from "../services/room/Update";
import RoomLogin from "../services/room/Login";
import RoomDelete from "../services/room/Delete";
import RoomShow from "../services/room/Show";
import RoomList from "../services/room/List";

import CollectionCreate from "../services/collection/Create";
import CollectionDelete from "../services/collection/Delete";
import CollectionUpdate from "../services/collection/Update";
import CollectionList from "../services/collection/List";

import ScriptCreate from "../services/script/Create";
import ScriptUpdate from "../services/script/Update";
import ScriptShow from "../services/script/Show";

import ExecutorCreate from "../services/executor/Create";
import ExecutorUpdate from "../services/executor/Update";


import config from "../../configs/config.json";

const router = new Router({ prefix: config.PREFIX });

// USER
router.post("/user", ServiceLayer.useService(UserCreate));

// SESSION
router.post("/session", ServiceLayer.useService(SessionCreate));

// ROOM
router.post("/user/:user_id/room", ServiceLayer.useService(RoomCreate));
router.patch("/user/:user_id/room/:roomId", ServiceLayer.useService(RoomUpdate));
router.post("/roomLogin", ServiceLayer.useService(RoomLogin));
router.get("/user/:user_id/room/:id", ServiceLayer.useService(RoomShow));
router.get("/user/:user_id/rooms", ServiceLayer.useService(RoomList));
router.delete("/user/:user_id/room/:id", ServiceLayer.useService(RoomDelete));

// COLECCTION
router.post("/user/room/:room_id/collection", ServiceLayer.useService(CollectionCreate));
router.get("/user/room/:room_id/collections", ServiceLayer.useService(CollectionList));
router.delete("/user/room/:room_id/collection/:id", ServiceLayer.useService(CollectionDelete));
router.patch("/user/room/:room_id/collection/:id", ServiceLayer.useService(CollectionUpdate));

// SCRIPT
router.post("/user/room/collection/:collection_id/executor/:executor_id/script", ServiceLayer.useService(ScriptCreate));

// SCRIPT
router.patch("/executor/script/:script_id", ServiceLayer.useService(ScriptUpdate));
router.get("/executor/:executor_id/script", ServiceLayer.useService(ScriptShow));

// EXECUTOR
router.post("/user/room/:room_id/executor", ServiceLayer.useService(ExecutorCreate));
router.patch("/user/room/:room_id/executor/:id", ServiceLayer.useService(ExecutorUpdate));


export default router;
