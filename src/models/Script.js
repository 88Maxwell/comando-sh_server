/* eslint-disable func-names */
export default function (sequelize, Sequelize) {
    const Script = sequelize.define("Script", {
        id           : { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
        collectionId : { type: Sequelize.UUID, allowNull: false, references: { model: "Collection", key: "id" }  },
        executorId   : { type: Sequelize.UUID, references: { model: "Executor", key: "id" } },
        data         : { type: Sequelize.STRING, allowNull: false },
        number       : { type: Sequelize.INTEGER, allowNull: true },
        status       : { type: Sequelize.ENUM, values: [ "SUCCESSFUL", "FAILED", "PENDING" ], defaultValue: "PENDING" }
    });

    Script.initRelation =  function () {
        const Collection = sequelize.model("Collection");
        const Executor = sequelize.model("Executor");

        this.belongsTo(Collection, { foreignKey: "id" });
        this.belongsTo(Executor, { foreignKey: "id" });
    };

    return Script;
}
