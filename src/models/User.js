/* eslint-disable func-names */
export default function (sequelize, Sequelize) {
    const User = sequelize.define("User", {
        id           : { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
        name         : { type: Sequelize.STRING },
        email        : { type: Sequelize.STRING, allowNull: false },
        passwordHash : { type: Sequelize.STRING },
        password     : { type : Sequelize.VIRTUAL,
            set(val) {
                this.setDataValue("passwordHash", this.encryptPassword(val));
            } }

    });

    User.initRelation =  function () {
        const Room = sequelize.model("Room");

        this.hasMany(Room, { foreignKey: "userId" });
    };

    return User;
}
