/* eslint-disable func-names */
export default function (sequelize, Sequelize) {
    const Room = sequelize.define("Room", {
        id           : { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
        userId       : { type: Sequelize.UUID, references: { model: "User", key: "id" } },
        name         : { type: Sequelize.STRING, allowNull: false },
        passwordHash : { type: Sequelize.STRING, allowNull: false },
        status       : { type: Sequelize.ENUM, values: [ "STOPED", "ACTIVE" ], defaultValue: "STOPED" },
        password     : {
            type : Sequelize.VIRTUAL,
            set(val) {
                this.setDataValue("passwordHash", this.encryptPassword(val));
            }
        }
    });

    Room.initRelation = function () {
        const User = sequelize.model("User");
        const Executor = sequelize.model("Executor");
        const Collection = sequelize.model("Collection");

        this.hasMany(Executor, { foreignKey: "roomId" });
        this.hasMany(Collection, { foreignKey: "roomId" });
        this.belongsTo(User, { foreignKey: "id" });
    };

    return Room;
}
