/* eslint-disable func-names */
export default function (sequelize, Sequelize) {
    const Executor = sequelize.define("Executor", {
        id          : { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
        roomId      : { type: Sequelize.UUID, allowNull: false, references: { model: "Room", key: "id" } },
        name        : { type: Sequelize.STRING },
        ip          : { type: Sequelize.STRING, allowNull: false },
        email       : { type: Sequelize.STRING, allowNull: false },
        isConnected : { type: Sequelize.BOOLEAN, defaultValue: false }
    });

    Executor.initRelation =  function () {
        const Room = sequelize.model("Room");
        const Script = sequelize.model("Script");

        this.hasMany(Script, { foreignKey: "executorId" });
        this.belongsTo(Room, { foreignKey: "id" });
    };

    return Executor;
}
