/* eslint-disable func-names */
export default function (sequelize, Sequelize) {
    const Collection = sequelize.define("Collection", {
        id     : { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
        roomId : { type: Sequelize.UUID, allowNull: false, references: { model: "Room", key: "id" } },
        status : { type: Sequelize.ENUM, values: [ "SUCCESSFUL", "FAILED", "PENDING" ], defaultValue: "PENDING" }
    });

    Collection.initRelation =  function () {
        const Room = sequelize.model("Room");
        const Script = sequelize.model("Script");

        this.hasMany(Script, { foreignKey: "collectionId" });
        this.belongsTo(Room, { foreignKey: "id" });
    };

    return Collection;
}
