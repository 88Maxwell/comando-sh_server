export const create = {
    data : {
        name     : "test",
        email    : "test.mail@mail.com",
        password : "password",
        status   : "STOPED"
    }
};

export const update = {
    data : { status: "ACTIVE" }
};

export const login = {
    data : {
        name     : "test",
        password : "password"
    }
};

export const createExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id     : [ "required" ],
        name   : { "is": "test" },
        userId : [ "required", "string" ],
        status : { "is": "STOPED" }
    } } ]
};

export const showExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id        : [ "required" ],
        name      : { "is": "test" },
        userId    : [ "required", "string" ],
        status    : { "is": "STOPED" },
        Executors : [ "required", "list_of_different_objects" ]
    } } ]
};

export const updateExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id     : [ "required" ],
        name   : { "is": "test" },
        userId : [ "required", "string" ],
        status : { "is": "ACTIVE" }
    } } ]
};

export const listExpect = {
    status : { "is": 200 },
    data   : [ "required", { "list_of_objects" : {
        id     : [ "required" ],
        name   : [ "required", "string" ],
        userId : [ "required", "string" ],
        status : { "is": "STOPED" }
    } } ]
};

export const wrongAuthDataExpect = {
    status : { "is": 500 },
    error  : [ "required", { "nested_object" : {
        code   : { "is": "AUTHENTICATION_FAILED" },
        fields : [ "required", { "nested_object" : {
            name     : { "is": "INVALID" },
            password : { "is": "INVALID" }
        } } ]
    } } ]
};
