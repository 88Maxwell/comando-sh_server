export const create = {
    data : {
        name     : "test",
        email    : "test.mail@mail.com",
        password : "password"
    }
};

export const createExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        token : [ "required", "string" ]
    } } ]
};

export const wrongAuthDataExpect = {
    status : { "is": 500 },
    error  : [ "required", { "nested_object" : {
        code   : { "is": "AUTHENTICATION_FAILED" },
        fields : [ "required", { "nested_object" : {
            email    : { "is": "INVALID" },
            password : { "is": "INVALID" }
        } } ]
    } } ]
};
