export const create = {
    data : {
        data   : "ls",
        status : "PENDING",
        number : 0
    }
};

export const update  = {
    data : {
        status : "SUCCESSFUL"
    }
};

export const createExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id           : [ "required" ],
        collectionId : [ "required", "string" ],
        executorId   : [ "required", "string" ],
        data         : [ "required", "string" ],
        number       : [ "required", "integer" ],
        status       : { "is": "PENDING" }
    } } ]
};

export const showExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id           : [ "required" ],
        collectionId : [ "required", "string" ],
        executorId   : [ "required", "string" ],
        data         : [ "required", "string" ],
        number       : [ "required", "integer" ],
        status       : { "is": "PENDING" }
    } } ]
};

export const updateExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id           : [ "required" ],
        collectionId : [ "required", "string" ],
        executorId   : [ "required", "string" ],
        data         : [ "required", "string" ],
        number       : [ "required", "integer" ],
        status       : { "is": "SUCCESSFUL" }
    } } ]
};
