export const create = {
    data : {
        name           : "executor",
        email          : "executor.mail@mail.com",
        passwordToRoom : "passwordToRoom",
        isConnected    : false,
        ip             : "123.45.56.234"
    }
};

export const createExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id          : [ "required" ],
        name        : { "is": "executor" },
        email       : { "is": "executor.mail@mail.com" },
        roomId      : [ "required", "string" ],
        ip          : [ "required", "string" ],
        isConnected : { "is": false }
    } } ]
};

export const updateEnableConnect = {
    data : {
        isConnected : true
    }
};

export const updateEnableConnectExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id          : [ "required" ],
        name        : { "is": "test" },
        email       : { "is": "executor.mail@mail.com" },
        roomId      : [ "required", "string" ],
        ip          : [ "required", "string" ],
        isConnected : { "is": true }
    } } ]
};

