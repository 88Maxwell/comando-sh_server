export const emptyData = { data: {} };

export const emptyDataExpect =  {
    status : { "is": 500 },
    error  : [ "required", { "nested_object" : {
        code   : { "is": "FORMAT_ERROR" },
        fields : [ "required" ]
    } } ]
};

export const wrongIdExpect = {
    status : { "is": 500 },
    error  : [ "required", { "nested_object" : {
        code   : { "is": "WRONG_ID" },
        fields : [ "required", { "nested_object" : {
            id : { "is": "WRONG_ID" }
        } } ]
    } } ]
};
