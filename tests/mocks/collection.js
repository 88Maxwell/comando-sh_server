export const createExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id     : [ "required" ],
        roomId : [ "required", "string" ],
        status : [ "string" ]
    } } ]
};

export const listExpect = {
    status : { "is": 200 },
    data   : [ "required", { "list_of_objects" : {
        id      : [ "required" ],
        roomId  : [ "required", "string" ],
        status  : [ "string" ],
        Scripts : [ "required", "list_of_different_objects" ]
    } } ]
};

export const updateToSuccessful = {
    data : {
        status : "SUCCESSFUL"
    }
};

export const updateToSuccessfulExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id     : [ "required" ],
        roomId : [ "required", "string" ],
        status : { "is": "SUCCESSFUL" }
    } } ]
};

