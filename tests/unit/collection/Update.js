import { request, cleanup, matchLIVR } from "../..";
import { updateToSuccessfulExpect, updateToSuccessful } from "../../mocks/collection";
import { generateUser } from "../../utils/user";
import { generateRoom } from "../../utils/room";
import { generateCollection } from "../../utils/collection";

suite("Update Collection");

let user;

let room;

let collection;

before(async () => {
    user = await generateUser();
    room = await generateRoom(user.id);
    collection = await generateCollection(room.id);
});

test("Positive : Collection update", async () => {
    await request
        .patch(`/api/v1/user/room/${room.id}/collection/${collection.id}`)
        .send(updateToSuccessful)
        .expect(200)
        .expect(res => matchLIVR(res.body, updateToSuccessfulExpect));
});

after(async () => {
    await cleanup();
});
