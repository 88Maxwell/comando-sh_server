import { request, cleanup, matchLIVR } from "../..";
import { createExpect } from "../../mocks/collection";
import { emptyData } from "../../mocks";
import { generateUser } from "../../utils/user";
import { generateRoom } from "../../utils/room";
import { generateCollection } from "../../utils/collection";

suite("Delete Collection");

let user;

let room;

let collection;

before(async () => {
    user = await generateUser();
    room = await generateRoom(user.id);
    collection = await generateCollection(room.id);
});

test("Positive : Delete Collection ", async () => {
    await request
        .delete(`/api/v1/user/room/${room.id}/collection/${collection.id}`)
        .send(emptyData)
        .expect(200)
        .expect(res => matchLIVR(res.body, createExpect));
});

after(async () => {
    await cleanup();
});
