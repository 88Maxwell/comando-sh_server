import { request, cleanup, matchLIVR } from "../..";
import { listExpect } from "../../mocks/collection";
import { generateUser } from "../../utils/user";
import { generateRoom } from "../../utils/room";
import { generateCollection } from "../../utils/collection";
import { generateScript } from "../../utils/script";
import { generateExecutor } from "../../utils/executor";

suite("List Collection");

let user;

let room;

before(async () => {
    user = await generateUser();
    room = await generateRoom(user.id);
    const collection = await generateCollection(room.id);
    const executor = await generateExecutor(room.id);

    await generateScript(collection.id, executor.id);
});

test("Positive : Collection List", async () => {
    await request
        .get(`/api/v1/user/room/${room.id}/collections`)
        .send()
        .expect(200)
        .expect(res => matchLIVR(res.body, listExpect));
});

after(async () => {
    await cleanup();
});
