import { request, cleanup, matchLIVR } from "../..";
import { createExpect } from "../../mocks/collection";
import { emptyData } from "../../mocks";
import { generateUser } from "../../utils/user";
import { generateRoom } from "../../utils/room";

suite("Create Collection");

let user;

let room;

before(async () => {
    user = await generateUser();
    room = await generateRoom(user.id);
});

test("Positive : Collection create", async () => {
    await request
        .post(`/api/v1/user/room/${room.id}/collection`)
        .send(emptyData)
        .expect(200)
        .expect(res => matchLIVR(res.body, createExpect));
});

after(async () => {
    await cleanup();
});
