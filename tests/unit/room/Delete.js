import { request, cleanup, matchLIVR } from "../..";
import { create, createExpect } from "../../mocks/room";
import { generateUser } from "../../utils/user";
import { generateRoom } from "../../utils/room";

suite("Delete Room");

let user;

let room;

before(async () => {
    user = await generateUser();
    room = await generateRoom(user.id);
});

test("Positive : Room delete", async () => {
    await request
        .delete(`/api/v1/user/${user.id}/room/${room.id}`)
        .send(create)
        .expect(200)
        .expect(res => matchLIVR(res.body, createExpect));
});

after(async () => {
    await cleanup();
});
