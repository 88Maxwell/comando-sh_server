import { request, cleanup, matchLIVR } from "../..";
import { showExpect } from "../../mocks/room";
import { generateUser } from "../../utils/user";
import { generateRoom } from "../../utils/room";

suite("Show Room");

let user;

let room;

before(async () => {
    user = await generateUser();
    room = await generateRoom(user.id);
});

test("Positive : Room show", async () => {
    await request
        .get(`/api/v1/user/${user.id}/room/${room.id}`)
        .send()
        .expect(200)
        .expect(res => matchLIVR(res.body, showExpect));
});

after(async () => {
    await cleanup();
});
