import { request, cleanup, matchLIVR } from "../..";
import { create, createExpect } from "../../mocks/room";
import { generateUser } from "../../utils/user";

suite("Create Room");

let user;

before(async () => {
    user = await generateUser();
});

test("Positive : Room create", async () => {
    await request
        .post(`/api/v1/user/${user.id}/room`)
        .send(create)
        .expect(200)
        .expect(res => matchLIVR(res.body, createExpect));
});

after(async () => {
    await cleanup();
});
