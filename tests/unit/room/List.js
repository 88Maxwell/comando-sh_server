import { request, cleanup, matchLIVR } from "../..";
import { listExpect } from "../../mocks/room";
import { generateUser } from "../../utils/user";
import { generateRooms } from "../../utils/room";

suite("List Room");

let user;

before(async () => {
    user = await generateUser();
    await generateRooms(user.id, 3);
});

test("Positive : Room list", async () => {
    await request
        .get(`/api/v1/user/${user.id}/rooms`)
        .send()
        .expect(200)
        .expect(res => matchLIVR(res.body, listExpect));
});

after(async () => {
    await cleanup();
});
