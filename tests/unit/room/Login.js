import { request, cleanup, matchLIVR } from "../..";
import { login, createExpect } from "../../mocks/room";
import { generateUser } from "../../utils/user";
import { generateRoom } from "../../utils/room";

suite("Login Room");

let user;


before(async () => {
    user = await generateUser();
    await generateRoom(user.id);
});

test("Positive : Room login", async () => {
    await request
        .post("/api/v1/roomLogin")
        .send(login)
        .expect(200)
        .expect(res => matchLIVR(res.body, createExpect));
});

test("Negative : Room login with not right password", async () => {
    await request
        .post("/api/v1/roomLogin")
        .send(login)
        .expect(200)
        .expect(res => matchLIVR(res.body, createExpect));
});

after(async () => {
    await cleanup();
});
