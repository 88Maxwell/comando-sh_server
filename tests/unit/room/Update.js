import { request, cleanup, matchLIVR } from "../..";
import { update, updateExpect } from "../../mocks/room";
import { generateUser } from "../../utils/user";
import { generateRoom } from "../../utils/room";

suite("Update Room");

let user;

let room;

before(async () => {
    user = await generateUser();
    room = await generateRoom(user.id);
});

test("Positive : Room Update", async () => {
    await request
        .patch(`/api/v1/user/${user.id}/room/${room.id}`)
        .send(update)
        .expect(200)
        .expect(res => matchLIVR(res.body, updateExpect));
});

after(async () => {
    await cleanup();
});
