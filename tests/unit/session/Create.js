import { request, cleanup, matchLIVR } from "../..";
import { create, createExpect, wrongAuthDataExpect } from "../../mocks/user";
import { generateUser } from "../../utils/user";

suite("Create session");

before(async () => {
    await generateUser("test");
});

test("Positive : Session create", async () => {
    await request
        .post("/api/v1/session")
        .send(create)
        .expect(200)
        .expect(res => matchLIVR(res.body, createExpect));
});

test("Negative : Create session with wrong password", async () => {
    await request
        .post("/api/v1/session")
        .send({ data: { ...create.data, password: "wrong-password" } })
        .expect(200)
        .expect(res => matchLIVR(res.body, wrongAuthDataExpect));
});

after(async () => {
    await cleanup();
});
