import { request, cleanup, matchLIVR } from "../..";
import { updateExpect, update } from "../../mocks/script";
import { generateUser } from "../../utils/user";
import { generateRoom } from "../../utils/room";
import { generateScript } from "../../utils/script";
import { generateCollection } from "../../utils/collection";
import { generateExecutor } from "../../utils/executor";

suite("Update script");

let script;

before(async () => {
    const user = await generateUser();
    const room = await generateRoom(user.id);
    const collection = await generateCollection(room.id);
    const executor = await generateExecutor(room.id);

    script = await generateScript(collection.id, executor.id);
});

test("Positive : Update Script ", async () => {
    await request
        .patch(`/api/v1/executor/script/${script.id}`)
        .send(update)
        .expect(200)
        .expect(res => matchLIVR(res.body, updateExpect));
});

after(async () => {
    await cleanup();
});
