import { request, cleanup, matchLIVR } from "../..";
import { createExpect, create } from "../../mocks/script";
import { generateUser } from "../../utils/user";
import { generateRoom } from "../../utils/room";
import { generateScript } from "../../utils/script";
import { generateCollection } from "../../utils/collection";
import { generateExecutor } from "../../utils/executor";

suite("Create script");

let collection;

let executor;

before(async () => {
    const user = await generateUser();
    const room = await generateRoom(user.id);

    collection = await generateCollection(room.id);
    executor = await generateExecutor(room.id);
    await generateScript(collection.id, executor.id);
});

test("Positive : Create Script ", async () => {
    await request
        .post(`/api/v1/user/room/collection/${collection.id}/executor/${executor.id}/script`)
        .send(create)
        .expect(200)
        .expect(res => matchLIVR(res.body, createExpect));
});

after(async () => {
    await cleanup();
});
