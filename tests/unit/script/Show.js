import { request, cleanup, matchLIVR } from "../..";
import { showExpect } from "../../mocks/script";
import { generateUser } from "../../utils/user";
import { generateRoom } from "../../utils/room";
import { generateScripts } from "../../utils/script";
import { generateCollection } from "../../utils/collection";
import { generateExecutor } from "../../utils/executor";

suite("Show script");

let collection;

let executor;

before(async () => {
    const user = await generateUser();
    const room = await generateRoom(user.id);

    collection = await generateCollection(room.id);
    executor = await generateExecutor(room.id);
    await generateScripts(collection.id, executor.id, 4);
});

test("Positive : Show Script ", async () => {
    await request
        .get(`/api/v1/executor/${executor.id}/script`)
        .send()
        .expect(200)
        .expect(res => matchLIVR(res.body, showExpect));
});

after(async () => {
    await cleanup();
});
