import { request, cleanup, matchLIVR } from "../..";
import { updateEnableConnect, updateEnableConnectExpect } from "../../mocks/executor";
import { generateUser } from "../../utils/user";
import { generateRoom } from "../../utils/room";
import { generateExecutor } from "../../utils/executor";

suite("Update Collection");

let user;

let room;

let executor;

before(async () => {
    user = await generateUser();
    room = await generateRoom(user.id);
    executor = await generateExecutor(room.id);
});

test("Positive : Collection update", async () => {
    await request
        .patch(`/api/v1/user/room/${room.id}/executor/${executor.id}`)
        .send(updateEnableConnect)
        .expect(200)
        .expect(res => matchLIVR(res.body, updateEnableConnectExpect));
});

after(async () => {
    await cleanup();
});
