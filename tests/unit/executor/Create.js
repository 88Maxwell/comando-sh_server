import { request, cleanup, matchLIVR } from "../..";
import { create, createExpect } from "../../mocks/executor";
import { generateUser } from "../../utils/user";
import { generateRoom } from "../../utils/room";

suite("Create Executor");

let user;

let room;

before(async () => {
    user = await generateUser();
    room = await generateRoom(user.id);
});

test("Positive : Executor create", async () => {
    await request
        .post(`/api/v1/user/room/${room.id}/executor`)
        .send(create)
        .expect(200)
        .expect(res => matchLIVR(res.body, createExpect));
});

after(async () => {
    await cleanup();
});
