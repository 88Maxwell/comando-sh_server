import supertest from "supertest";
import LIVR from "livr/lib/LIVR";
import extraRules from "livr-extra-rules";
import { assert, AssertionError } from "chai";
import inspect from "util-inspect";

import serverCallback from "../src";
import sequelize from "../src/sequelize";

LIVR.Validator.registerDefaultRules(extraRules);

export const request = supertest.agent(serverCallback);

export function matchLIVR(got, expected) {
    try {
        const validator = new LIVR.Validator(expected);
        const validData = validator.validate(got);

        assert.ok(validData && expected, `Error: ${inspect(validator.getErrors())}`);
    } catch (err) {
        console.log("got:", got, "\n");
        console.log("expected:", expected, "\n");
        if (!(err instanceof AssertionError)) console.log(err);
        throw err;
    }
}

export async function cleanup() {
    await sequelize.models.Script.destroy({ where: {} });
    await sequelize.models.Executor.destroy({ where: {} });
    await sequelize.models.Collection.destroy({ where: {} });
    await sequelize.models.Room.destroy({ where: {} });
    await sequelize.models.User.destroy({ where: {} });

    serverCallback.close();
}
