import { executor as executorDump } from "../../src/utils/services/dumps";

import { create } from "../mocks/executor";
import sequelize from "../../src/sequelize";

const Executor = sequelize.model("Executor");

export async function generateExecutor(roomId, name = "test") {
    const executor = await Executor.create({ ...create.data, name, roomId });

    return { ...executorDump(executor) };
}

export async function generateExecutors(roomId, count = 1) {
    const executorList = [];

    // eslint-disable-next-line
    for (let index = 0; index < count; index++) {
        executorList.push(await generateExecutor(roomId, `test ${index}`));
    }

    return executorList;
}
