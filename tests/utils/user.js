import { user as userDump } from "../../src/utils/services/dumps";
import { create } from "../mocks/user";
import sequelize from "../../src/sequelize";

const User = sequelize.model("User");

export async function generateUser(name = "test") {
    const user = await User.create({ ...create.data, name });

    return { ...userDump(user) };
}

export async function generateUsers(count = 1) {
    const userList = [];

    // eslint-disable-next-line
    for (let index = 0; index < count; index++) {
        userList.push(await generateUser(`test ${index}`));
    }

    return userList;
}
