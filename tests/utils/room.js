import { room as roomDump } from "../../src/utils/services/dumps";

import { create } from "../mocks/user";
import sequelize from "../../src/sequelize";

const Room = sequelize.model("Room");

export async function generateRoom(userId, name = "test") {
    const room = await Room.create({ ...create.data, name, userId });

    return { ...roomDump(room) };
}

export async function generateRooms(userId, count = 1) {
    const roomList = [];

    // eslint-disable-next-line
    for (let index = 0; index < count; index++) {
        roomList.push(await generateRoom(userId, `test ${index}`));
    }

    return roomList;
}
