import { collection as collectionDump } from "../../src/utils/services/dumps";

import { create } from "../mocks/user";
import sequelize from "../../src/sequelize";

const Collection = sequelize.model("Collection");

export async function generateCollection(roomId, name = "test") {
    const collection = await Collection.create({ ...create.data, name, roomId });

    return { ...collectionDump(collection) };
}

export async function generateCollections(roomId, count = 1) {
    const collectionList = [];

    // eslint-disable-next-line
    for (let index = 0; index < count; index++) {
        collectionList.push(await generateCollection(roomId, `test ${index}`));
    }

    return collectionList;
}
