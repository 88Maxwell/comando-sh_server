import { script as scriptDump } from "../../src/utils/services/dumps";

import { create } from "../mocks/script";
import sequelize from "../../src/sequelize";

const Script = sequelize.model("Script");

export async function generateScript(collectionId, executorId) {
    const script = await Script.create({ ...create.data, collectionId, executorId });

    return { ...scriptDump(script) };
}

export async function generateScripts(collectionId, executorId, count = 1) {
    const scriptList = [];

    // eslint-disable-next-line
    for (let index = 0; index < count; index++) {
        scriptList.push(await generateScript(collectionId, executorId));
    }

    return scriptList;
}
