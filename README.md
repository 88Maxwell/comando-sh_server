# Boilerplate[backend]

## Initialize

### Development
1. yarn 
2. cp configs/config.json.sample configs/config.json 
3. cp configs/db.json.sample configs/db.json 
4. yarn dev

### Test
1. yarn 
2. cp configs/config.json.sample configs/config.json 
3. cp configs/db.json.sample configs/db.json 
4. yarn test:lint
5. yarn test:unit

### Production
1. yarn 
2. cp configs/config.json.sample configs/config.json 
3. cp configs/db.json.sample configs/db.json 
4. yarn start
